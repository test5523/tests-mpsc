CMAKE=cmake
CURDIR=$PWD

echo =======================================================
echo =============== Build release =========================
echo =======================================================

BLDDIR=../.build/tests-mpsc/Release

if [ ! -d $BLDDIR ]; then
  mkdir -p $BLDDIR
fi

cd $BLDDIR
$CMAKE --build . -- -j4 $*
cd $CURDIR

if [ -f $BLDDIR/tests-mpsc  ]; then
  cp $BLDDIR/tests-mpsc .
fi
