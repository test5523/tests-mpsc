#include <limits>
#include <vector>
#include <memory>
#include <algorithm>
#include <iostream>
#include "exn_queue_mpsc.h"
#include "exn_proc_mpsc.h"
#include "exn_proc_mpsc_mthread.h"
#include "exn_proc_mpsc_nthread.h"

#include "gtest/gtest.h"
using ::testing::EmptyTestEventListener;
using ::testing::InitGoogleTest;
using ::testing::Test;
using ::testing::TestEventListeners;
using ::testing::TestInfo;
using ::testing::TestPartResult;
using ::testing::UnitTest;

/** Queue_mpsc test data struct **/
struct queue_mpsc_data_t
{
  /** thread id **/
  size_t thread_id;
  /** push counter **/
  size_t push_cnt;
};

/** Consumer test object that check data coherence for multi producers **/
struct consumer_mpsc_test_t : public exn::consumer<queue_mpsc_data_t>
{
  bool                _isok;
  std::vector<size_t> _counter;
  typedef std::shared_ptr<consumer_mpsc_test_t> ptr_t;

  consumer_mpsc_test_t(const size_t producers_num)
    :_isok(true)
  {
    _counter.resize(producers_num);
    std::fill(_counter.begin(),_counter.end(),0); 
  }

  void ondata(queue_mpsc_data_t &val)
  {
    if (val.thread_id < _counter.size())
    {
      _isok = (_counter[val.thread_id] != val.push_cnt) ?false :_isok;
      _counter[val.thread_id]++;
    }
  }

  bool check()
  {
    return _isok;
  }

};

typedef exn::queue_mpsc<queue_mpsc_data_t> queue_mpsc_test_t;
typedef exn::producer<queue_mpsc_data_t>   producer_mpsc_test_t;

/** Check if no memory for queue buffer & exception occure **/
TEST(test_queue_mpsc, exception_if_queue_max_size)
{
  bool isok = false;

  queue_mpsc_test_t::ptr_t queue;
  size_t queue_size = std::numeric_limits<size_t>::max();
  /** try to allocate queue buffer with max size of type size_t **/
  EXPECT_ANY_THROW( queue = exn::queue_mpsc_make_ptr<queue_mpsc_data_t>(queue_size) );
}

/** Check subscride unsubscribe while push to queue **/
TEST(test_queue_mpsc, subscribe_unsubscribe)
{
  const size_t queue_size = 32;
  std::shared_ptr<consumer_mpsc_test_t> consumer = std::make_shared<consumer_mpsc_test_t>(1);  
  queue_mpsc_test_t::ptr_t queue = exn::queue_mpsc_make_ptr<queue_mpsc_data_t>(queue_size);
  producer_mpsc_test_t::ptr_t producer = exn::queue_mpsc_producer_get<queue_mpsc_data_t>(queue);

  /** create one producer thread **/
  std::thread producer_thread( 
    [&producer, queue_size]
    {
      for (size_t i = 1; i <= queue_size; i++)
      {
        queue_mpsc_data_t val = {0,i};
        producer->insert(val);

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
      }
    }
  );

  /** subscribe / unsubscribe several times **/
  EXPECT_EQ( true, queue->subscribe( consumer) );
  EXPECT_EQ( false,queue->subscribe( consumer) );
  std::this_thread::sleep_for(std::chrono::milliseconds(4));
  EXPECT_EQ( true, queue->unsubscribe());
  EXPECT_EQ( true, queue->subscribe( consumer ));
  EXPECT_EQ( true, queue->unsubscribe());

  /** wait producer thread end **/
  producer_thread.join();

  /** counter must be less the queue_size but more then 0 **/
  EXPECT_LE( 0,         consumer->_counter[0] );
  EXPECT_GE( queue_size,consumer->_counter[0] );
}

/** Check data coherence while pushing from multiple threads **/
TEST(test_queue_mpsc, coherence_from_multiple_producers)
{
  const size_t queue_size = 16;
  const size_t producers_num = 8;
  const size_t insert_per_producer_num  = 1000;
  std::shared_ptr<consumer_mpsc_test_t> consumer = std::make_shared<consumer_mpsc_test_t>(1);  
  queue_mpsc_test_t::ptr_t queue = exn::queue_mpsc_make_ptr<queue_mpsc_data_t>(queue_size);

  /** consumer subscribe for queue **/
  EXPECT_EQ(true, queue->subscribe(consumer));

  /** create producers & producer's threads **/
  std::vector< std::unique_ptr<std::thread> > vthreads;
  for (size_t n = 0; n < producers_num; n++)
  {
    /** get queue producer **/
    producer_mpsc_test_t::ptr_t producer = exn::queue_mpsc_producer_get<queue_mpsc_data_t>(queue);
  
    /** create producer thread **/
    std::unique_ptr<std::thread> pthread (
    new std::thread
    (
      [ producer, n, insert_per_producer_num]
      {
        for (size_t i = 0; i < insert_per_producer_num; i++)
        {
          queue_mpsc_data_t val = {n,i};
          producer->insert(val);
        }
      }
    )
    );

    vthreads.push_back( std::move(pthread) );
  }

  /** wait producer threads **/
  for (auto &pthread :vthreads)
  {
    pthread->join();
  }

  /** check coherence **/
  EXPECT_EQ( true, consumer->check() );
}

/** Check map container for std::shared_ptr of queue_mpsc  **/
TEST(test_queue_mpsc, map_container_for_shared_ptr )
{
  const size_t map_size = 8;
  const size_t queue_size = 16;

  std::map< size_t, queue_mpsc_test_t::ptr_t > queue_map;

  for (size_t n = 0; n < map_size; n++)
  {
    queue_mpsc_test_t::ptr_t queue = exn::queue_mpsc_make_ptr<queue_mpsc_data_t>(queue_size);

    queue_map[n] = queue;
  }

}

template < typename key_t, typename data_t >
void multiple_producers_insert_to_processor(
  exn::proc_mpsc<key_t, data_t> &processor,  
  const size_t producers_num,
  const size_t consumers_num,
  const size_t insert_per_producer_num
)
{
  std::vector<consumer_mpsc_test_t::ptr_t>     vconsumers;
  std::vector< std::unique_ptr<std::thread> >  vthreads;

  /** Add consumers **/
  for (size_t id = 0; id < consumers_num; id++)
  {
    /** create consumer **/
    consumer_mpsc_test_t::ptr_t consumer = std::make_shared<consumer_mpsc_test_t>(producers_num);
    EXPECT_EQ(true, bool(consumer));
    if (!consumer) continue;
    /** subscriber for processor **/
    processor.consumer_subscribe(id, consumer);
    /** store consumer **/
    vconsumers.push_back(std::move(consumer));
  }

  /** Add producer threads for every consumer **/
  for (size_t id = 0; id < consumers_num; id++)
  {
    for (size_t n = 0; n < producers_num; n++)
    {
      producer_mpsc_test_t::ptr_t producer = processor.producer_get(id);
      EXPECT_EQ(true, bool(producer));
      if (!producer) continue;

      /** create producer thread **/
      std::unique_ptr<std::thread> pthread (
      new std::thread
      (
        [ producer, n, insert_per_producer_num]
        {
          for (size_t i = 0; i < insert_per_producer_num; i++)
          {
            queue_mpsc_data_t val = {n,i};
            producer->insert(val);
          }
        }
      )
      );

      vthreads.push_back( std::move(pthread) );
    }
  }

  /** wait producer threads **/
  for (auto &pthread :vthreads)
  {
    pthread->join();
  }

  /** check coherence **/
/*
  for (auto &consumer : vconsumers)
  {
    EXPECT_EQ( true, consumer->check() );
  }
*/
}

TEST(test_proc, multiple_producers_insert_to_mthread)
{
  const size_t queue_size = 16;
  const size_t producers_num = 4;
  const size_t consumers_num = 4;
  const size_t insert_per_producer_num  = 10000;
  
  exn::proc_mpsc_mthread<size_t , queue_mpsc_data_t> processor(queue_size);

  multiple_producers_insert_to_processor<size_t, queue_mpsc_data_t>
    (processor, producers_num, consumers_num, insert_per_producer_num);
}

TEST(test_proc, multiple_producers_insert_to_nthread)
{
  const size_t queue_size = 16;
  const size_t nthreads   = 4;
  const size_t producers_num = 4;
  const size_t consumers_num = 8;
  const size_t insert_per_producer_num  = 10000;

  exn::proc_mpsc_nthread<size_t , queue_mpsc_data_t> processor(nthreads, queue_size);

  multiple_producers_insert_to_processor<size_t, queue_mpsc_data_t>
    (processor, producers_num, consumers_num, insert_per_producer_num);
}

int main(int argc, char* argv[])
{
  InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
