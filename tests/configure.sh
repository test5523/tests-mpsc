CMAKE=cmake
CURDIR=$PWD

echo =======================================================
echo =============== Configure debug =======================
echo =======================================================

BLDDIR=../.build/tests-mpsc/Debug

if [ ! -d $BLDDIR ]; then
  mkdir -p $BLDDIR
fi
cd $BLDDIR

$CMAKE \
 -DCMAKE_BUILD_TYPE=Debug \
 -Wno-dev \
 $CURDIR

cd $CURDIR

echo =======================================================
echo =============== Configure release =====================
echo =======================================================

BLDDIR=../.build/tests-mpsc/Release

if [ ! -d $BLDDIR ]; then
  mkdir -p $BLDDIR
fi
cd $BLDDIR

$CMAKE \
 -DCMAKE_BUILD_TYPE=Release \
 -Wno-dev \
 $CURDIR

cd $CURDIR
