#pragma once

#include <deque>
#include <mutex>
#include <memory>
#include "exn_queue_mpsc_inl.h"

namespace exn
{

/** @brief Producer interface for insertion in queue **/
template <typename data_t>
struct producer
{
  /** @brief Shared_ptr type for producer interface **/
  typedef std::shared_ptr< producer<data_t> > ptr_t;

  virtual ~producer(){}

  /** @brief Push data to queue (thread-safe) **/
  virtual void insert(const data_t &val)=0;
};

/** @brief Consumer interface for parsing queue **/
template <typename data_t>
struct consumer
{
  /** @brief Shared_ptr type for consumer interface **/
  typedef std::shared_ptr< consumer<data_t> > ptr_t;

  virtual ~consumer(){}

  /** @brief Function called to parse data queue **/
  virtual void ondata(data_t &val)=0;
};

/** @brief Multi Producer Single Consumer interface
 *  - Insertion in the queue can occur from multiple producers (thread-safe)
 *  - Available to subscribe / unsubscribe to parsing the queue by one fixed consumer (one queue - one consumer)
 *  - Increasing the number of queues and consumers does not lead to a significant drop in productivity
 *  
 *  queue_mpsc allocate circular buffer .
 *  **/
template<typename data_t>
struct queue_mpsc : public queue_multi_producer_single_consumer<data_t>, public producer<data_t>
{
  typedef std::shared_ptr< queue_mpsc<data_t>  > ptr_t;

  /** @brief Create Multi Producer Single Consumer Queue 
   *  Allocate circular buffer, create synchonization objects and one consumer thread
   *  @param max_size - maximum buffer size
   *  @throw bab_alloc if not enough memmory **/
  queue_mpsc(size_t max_size)
   :queue_multi_producer_single_consumer<data_t>(max_size)
  {}

  /** @brief Destroy Queue
   *  Stop consumer thread, delete synchronization objects, free circular buffer **/
  virtual ~queue_mpsc()
  { }

  /** std::move doesn't support **/
  queue_mpsc(const queue_mpsc&) = delete;
  queue_mpsc& operator =(const queue_mpsc&) = delete;
  queue_mpsc(const queue_mpsc&&) = delete;
  queue_mpsc& operator =(const queue_mpsc&&) = delete;

  /** @brief Subscribe for parsing the queue 
   *  @return true on success, false if consumer is already subscribed **/
  bool subscribe(typename consumer<data_t>::ptr_t consumer_base)
  {return this->_subscribe(consumer_base); }

  /** @brief Unsubscribe from parsing the queue 
   *  @return true on success **/
  bool unsubscribe()
  { return this->_unsubscribe(); }

  /** @brief Insert request to the queue (thread-safe) **/
  void insert(const data_t &val)
  { this->push(val);}
}; 

/** @brief Create queue_mpsc shared_ptr **/
template<typename data_t>
typename queue_mpsc<data_t>::ptr_t queue_mpsc_make_ptr(size_t max_size)
{
  return std::make_shared< queue_mpsc<data_t> >(max_size);
}

/** @brief Get producer shred_ptr from queue shared_ptr **/
template<typename data_t>
typename producer<data_t>::ptr_t queue_mpsc_producer_get(typename queue_mpsc<data_t>::ptr_t queue_mpss_base)
{
  return std::static_pointer_cast< producer<data_t> >(queue_mpss_base);
}

} /** namespace exn **/
