#pragma once
#include "exn_queue_mpsc.h"

namespace exn
{
/** @brief Queue Processor Interface **/
template < typename key_t, typename data_t >
class proc_mpsc
{
public:

  using producer_ptr = typename producer<data_t>::ptr_t;
  using consumer_ptr = typename consumer<data_t>::ptr_t;

  /** @brief Get producer interface
   *  @param key - queue idenficator
   *  @return producer interface **/
  virtual producer_ptr producer_get(const key_t key) = 0;

  /** @brief Subscriber consumer on queue
   *  @param key - queue idenficator
   *  @return true - if success, false - overwise **/
  virtual bool consumer_subscribe(const key_t key, consumer_ptr pconsumer) = 0;

  /** @brief Unsubscribe consumer from queue
   *  @param key - queue idenficator
   *  @return true - if success, false - overwise **/
  virtual bool consumer_unsubscribe(const key_t key) = 0;
};

}