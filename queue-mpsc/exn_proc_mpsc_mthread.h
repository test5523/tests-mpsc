#pragma once
#include <mutex>
#include <unordered_map> 
#include "exn_proc_mpsc.h"

namespace exn
{

/** @brief Queue Processor with Multi Threads 
 *  Creates queue & thread for every consumer  **/
template < typename key_t, typename data_t >
class proc_mpsc_mthread : public proc_mpsc<key_t, data_t>
{
  using queue_ptr    = typename queue_mpsc<data_t>::ptr_t;
  using producer_ptr = typename producer<data_t>::ptr_t;
  using consumer_ptr = typename consumer<data_t>::ptr_t;

  /** Maximum buffer size for queue */
  size_t                                _max_size;
  /** Queue container **/
  std::unordered_map<key_t, queue_ptr > _map_proc;
  /** Mutex for queue container **/
  std::mutex                            _mtx_proc;

  /** std::move doesn't support **/
  proc_mpsc_mthread(const proc_mpsc_mthread&) = delete;
  proc_mpsc_mthread& operator =(const proc_mpsc_mthread&) = delete;
  proc_mpsc_mthread(const proc_mpsc_mthread&&) = delete;
  proc_mpsc_mthread& operator =(const proc_mpsc_mthread&&) = delete;

  /** No default constructor **/
  proc_mpsc_mthread()
  {}
public:
  proc_mpsc_mthread(size_t max_size)
    :_max_size(max_size)
  {}
  ~proc_mpsc_mthread()
  {}

  producer_ptr producer_get(const key_t key)
  {
    queue_ptr queue;

    std::unique_lock<std::mutex> lock(_mtx_proc);
    /** find queue **/
    auto it = _map_proc.find(key);
    /** if queue doesn't exist **/
    if (it == _map_proc.end())
    {
      /** then create one **/
      queue = std::make_shared< queue_mpsc<data_t> >(_max_size);

      /** if create then add to queue map **/
      if (queue)
        _map_proc[key] = queue;
    }
    else
      queue = it->second;

    return std::static_pointer_cast< producer<data_t> >(queue);
  }

  bool consumer_subscribe(const key_t key, consumer_ptr pconsumer)
  {
    bool isok = false;
    queue_ptr queue;

    std::unique_lock<std::mutex> lock(_mtx_proc);
    /** find queue **/
    auto it = _map_proc.find(key);
    /** if queue doesn't exist **/
    if (it == _map_proc.end())
    {
      /** then create one **/
      queue = std::make_shared< queue_mpsc<data_t> >(_max_size);

      /** if created then add to queue map **/
      if (queue)
        _map_proc[key] = queue;
    }
    else
      queue = it->second;

    /** if queue exist **/
    if (bool(queue))
      /** then subscribe **/
      isok = queue->subscribe(pconsumer);

    return isok;
  }

  bool consumer_unsubscribe(const key_t key)
  {
    bool isok = false;

    std::unique_lock<std::mutex> lock(_mtx_proc);
    /** find queue **/
    auto it = _map_proc.find(key);
    /** if queue exist **/
    if (it == _map_proc.end())
    {
      /** then unsubscriber **/
      isok = it->second->unsubscribe();
    }

    return isok;
  }
};

}