#pragma once
#include <mutex>
#include <vector>
#include <unordered_map> 
#include "exn_proc_mpsc.h"

namespace exn
{

/** @brief Queue Processor with N Threads 
 *  Creates N queue & thread **/
template < typename key_t, typename data_t >
class proc_mpsc_nthread : public proc_mpsc<key_t, data_t>
{
  struct data_nthread_t
  {
    key_t    _key;
    data_t   _val;
  };

  using producer_t    = producer<data_nthread_t>;
  using queue_ptr     = typename queue_mpsc<data_nthread_t>::ptr_t;
  using producer_ptr  = typename producer<data_t>::ptr_t;
  using consumer_ptr  = typename consumer<data_t>::ptr_t;

  class producer_nthread : public producer<data_t>
  {
    std::function<void (const data_t &) > _finsert;

  public:

    producer_nthread(std::function< void (const data_t &) > finsert)
      :_finsert(finsert)
    {}

    /** @brief Push data to queue (thread-safe) **/
    void insert(const data_t &val)
    {
      _finsert(val);
    }
  };
  class consumer_nthread : public consumer<data_nthread_t>
  {
    std::function< void (data_nthread_t &)> _ondata;
  public:
    consumer_nthread(std::function<void (data_nthread_t &)> ondata)
      :_ondata(ondata)
    {}
    /** @brief Function called to parse data queue **/
    void ondata(data_nthread_t &val)
    {
      _ondata(val);
    }
  };

  struct nthread_t
  {
    /** Thread Queue **/
    queue_ptr                   _queue;
    /** Mutex for every thread consumer **/
    std::shared_ptr<std::mutex> _mtx;
    /** Consumer for every thread **/
    typename consumer_nthread::ptr_t _consumer;
  };

  struct consumer_t
  {
    /** Consumer intreface **/
    consumer_ptr                    _pconsumer;
    /** Consumer mutex (to block mutual consumer execution from nthreads) **/
    std::shared_ptr<std::mutex>     _pmutex;
  };

  struct lock_nthread
  {
    std::vector<nthread_t> &_vec_nthread; 

    lock_nthread(std::vector<nthread_t> &vec_nthread)
      :_vec_nthread(vec_nthread)
    {
      for (auto &th : _vec_nthread)
        th._mtx->lock();
    }
    ~lock_nthread()
    {
      for (auto &th : _vec_nthread)
        th._mtx->unlock();
    }
  };

  /** Counter for insert operation, it needs for equally push value for every thread queue **/
  size_t                                   _cnt_insert;
  std::vector<nthread_t>                   _vec_nthread;
  /** Map key to consumer data **/
  std::unordered_map<key_t, consumer_t >   _map_consumer;

  /** No default constructor **/
  proc_mpsc_nthread()
  {}

  /** Process value from threads queue **/
  void nthread_consumer_ondata(size_t nthread, data_nthread_t &nthread_val)
  {
    auto &th = _vec_nthread[nthread];

    std::unique_lock<std::mutex> lock(*th._mtx);

    /** find consumer **/
    auto it = _map_consumer.find(nthread_val._key);
    /** if consumer exist **/
    if (it != _map_consumer.end())
    {
      /** only one cunsumer can be called from any threads **/
      std::unique_lock<std::mutex> lock_consumer(*it->second._pmutex);
      /** then call consumer callback function **/
      it->second._pconsumer->ondata(nthread_val._val);
    }
  }

  /** Insert value to threads queue **/
  void nthread_producer_insert(key_t key, const data_t &val)
  {
    data_nthread_t nthread_val = {key,val};

    /** equally push value in all thread queue **/
    size_t nthread = (_cnt_insert++)%_vec_nthread.size();
    _vec_nthread[nthread]._queue->insert( nthread_val );
  }

public:

  /** Create N thread queue for all consumers
   *  @param nthread - thread queue number 
   *  @param queue_max_size - maximum number of insert requests for all consumers
   *  @throw bad_alloc, invalid_argument **/
  proc_mpsc_nthread(size_t nthread, size_t queue_max_size)
    :_cnt_insert(0)
  {
    if (0 == nthread)
      throw std::invalid_argument("nthread mast be more then 1");

    /** create N threads **/
    for (size_t n = 0; n < nthread; n++)
    {
      nthread_t data;

      /** create thread queue  **/
      data._queue = std::make_shared< queue_mpsc<data_nthread_t> >(queue_max_size);
      /** create thread consumer **/
      data._consumer = std::make_shared< consumer_nthread >
        ( [this,n](data_nthread_t &nthread_val){ this->nthread_consumer_ondata(n,nthread_val); } );
      /** create thread mutex **/
      data._mtx = std::make_shared< std::mutex >();

      if (data._queue && data._consumer && data._mtx)
      {
        /** subscribe thread consumer for thread queue **/
        data._queue->subscribe(data._consumer);

        _vec_nthread.push_back(data);
      }
    }
  }

  /** @brief Get producer interface
   *  @param key - queue identificator
   *  @return producer interface **/
  producer_ptr producer_get(const key_t key)
  {
    /** Create producer with key param **/
    return std::make_shared<producer_nthread>
      ( [this,key](const data_t &val) {this->nthread_producer_insert(key,val);} );
  }

  /** @brief Subscriber consumer on queue
   *  @param key - queue idenficator
   *  @return true - if success, false - overwise **/
  bool consumer_subscribe(const key_t key, consumer_ptr pconsumer)
  {
    bool isok = false;

    /** block all threads **/
    lock_nthread lock(_vec_nthread);

    /** subscribe **/
    {
      /** find consumer **/
      auto it = _map_consumer.find(key);
      /** if consumer doesn't exist **/
      if (it == _map_consumer.end())
      {
        /** add one **/
        consumer_t consumer;
        consumer._pconsumer = pconsumer;
        consumer._pmutex = std::make_shared<std::mutex>();
        _map_consumer[key] = consumer;
        isok = true;
      }
    }
    
    return isok;
  }

  /** @brief Unsubscribe consumer from queue
   *  @param key - queue idenficator
   *  @return true - if success, false - overwise **/
  bool consumer_unsubscribe(const key_t key)
  {
    bool isok = false;

    /** block all threads **/
    lock_nthread lock(_vec_nthread);

    /** unsubscribe **/
    {
      /** find consumer **/
      auto it = _map_consumer.find(key);
      /** if consumer exist **/
      if (it != _map_consumer.end())
      {
        /** then erase **/
        _map_consumer.erase(it);
        isok = true;
      }
    }
    
    return isok;
  }

};

}