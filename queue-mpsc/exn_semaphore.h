#pragma once

#include <thread>
#include <mutex>
#include <condition_variable>

namespace exn 
{
class semaphore
{
private:
  std::mutex              _mutex;
  std::condition_variable _cv;
  int                     _count;

public:
  semaphore (int count_ = 0)
  : _count(count_) 
  {
  }
  ~semaphore () 
  {
  }

  // Mutex and condition variables are not movable and there is no need for smart pointers yet
  semaphore(const semaphore&) = delete;
  semaphore& operator =(const semaphore&) = delete;
  semaphore(const semaphore&&) = delete;
  semaphore& operator =(const semaphore&&) = delete;

  inline void notify(  )
  {
    std::unique_lock<std::mutex> lock(_mutex);
    _count++;
    //notify the waiting thread
    _cv.notify_one();
  }
  inline void wait(  ) 
  {
    std::unique_lock<std::mutex> lock(_mutex);
    while(_count == 0)
    {
      //wait on the mutex until notify is called
      _cv.wait(lock);
    }
    _count--;
  }
};
}
