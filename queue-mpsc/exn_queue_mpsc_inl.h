#pragma once
#include <thread>
#include <functional>
#include <boost/circular_buffer.hpp>
#include "exn_semaphore.h"

namespace exn
{

/** @brief Forward consumer interface for parsing queue **/
template <typename data_t>
struct consumer;

/** @brief Multi producer single consumer queue
 *  The object create thread that call consumer function on every insretion from
 *  multiple producers. Insertion in queue from multiple producer made by component
 *  <boost/lockfree/queue.hpp> is efficient, simple and safe for call form multiple threads
 *  Semaphore is signaled on every insretion from producer. 
 *
 *  **/
template <typename data_t>
class queue_multi_producer_single_consumer
{
  typedef std::function<void (data_t &)> consumer_func_t;

  boost::circular_buffer<data_t> _queue;
  exn::semaphore                _sempush;
  exn::semaphore                _sempull;

  /** mutex for subscribe/unsubscribe **/
  std::mutex                    _mutex_subscribe;
  /** mutex for queue push/pull **/
  std::mutex                    _mutex_queue;
  /** pointer to consumer function **/
  typename consumer<data_t>::ptr_t _pconsumer;

  /** consumer thread exit flag **/
  bool                          _thread_exit_flag;
  /** consumer thread **/
  std::thread                   _thread;

  void thread_consumer_main()
  {
    while (true)
    {
      /** wait signal from producer **/
      _sempull.wait();
      if (_thread_exit_flag)
        break;

      data_t val;

      /** pull data from queue **/
      {
        std::unique_lock<std::mutex> lock(_mutex_queue);
        val = _queue.front();
        _queue.pop_front();
      }

      /** call consumer callback function **/
      try
      {
        std::unique_lock<std::mutex> lock(_mutex_subscribe);
        if (! (nullptr == _pconsumer) )
          _pconsumer->ondata(val);
      }
      catch(...)
      {}

      /** notify to producer that there is free space **/
      _sempush.notify();
    }
  }

  /** No default constructor **/
  queue_multi_producer_single_consumer()
  {}
public:
  queue_multi_producer_single_consumer(const size_t max_size)
    :_queue(max_size)
    ,_sempush(max_size)
    ,_sempull(0)
    ,_pconsumer(nullptr)
    ,_thread_exit_flag(false)
    ,_thread( [this]{ this->thread_consumer_main(); } )
  {}

  /** std::move doesn't support **/
  queue_multi_producer_single_consumer(const queue_multi_producer_single_consumer&) = delete;
  queue_multi_producer_single_consumer& operator =(const queue_multi_producer_single_consumer&) = delete;
  queue_multi_producer_single_consumer(const queue_multi_producer_single_consumer&&) = delete;
  queue_multi_producer_single_consumer& operator =(const queue_multi_producer_single_consumer&&) = delete;

  /** stop thread consumer **/
  ~queue_multi_producer_single_consumer()
  {
    _thread_exit_flag = true;
    _sempull.notify();
    _thread.join();
  }

  /** push data in queue for consumer **/
  void push( const data_t &val )
  {
    _sempush.wait();

    std::unique_lock<std::mutex> lock(_mutex_queue);
    _queue.push_back(val);

    _sempull.notify();
  }
protected:
  /** consumer subscribe **/
  bool _subscribe(typename consumer<data_t>::ptr_t &consumer_base)
  {
    bool issubscribe = false;
    std::unique_lock<std::mutex> lock(_mutex_subscribe);
    if (!_pconsumer)
    { _pconsumer = consumer_base; issubscribe = true; }

    return issubscribe;
  }

  /** consumer unsubscribe **/
  bool _unsubscribe()
  {
    std::unique_lock<std::mutex> lock(_mutex_subscribe);
    _pconsumer.reset();
    return true;
  }
};

}